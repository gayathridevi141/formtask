import { Form, Button } from "react-bootstrap"

import {EmployeeContext} from './contexts/FormContext';
import {useContext, useState} from 'react';



const AddForm = () =>{

    const {addEmployee} = useContext(EmployeeContext);

    const [newEmployee, setNewEmployee] = useState({
        salutation:"", firstname:"", lastname:"", email:"", date:"", address:""
    });

    const onInputChange = (e) => {
        setNewEmployee({...newEmployee,[e.target.name]: e.target.value})
    }

    const {salutation , firstname, lastname, email, date, address} = newEmployee;

    const handleSubmit = (e) => {
        e.preventDefault();
        addEmployee(salutation ,firstname, lastname, email, date, address);
    }

     return (

        <Form onSubmit={handleSubmit}>


<Form.Group>
<div className="form-control">
                    <label>Salutation:</label>
                <select name="salutation" value={salutation}  onChange = { (e) => onInputChange(e)}>
                    <option value="None"  onChange = { (e) => onInputChange(e)}>None</option>
                    <option value="eg.."  onChange = { (e) => onInputChange(e)}>1</option>
                    <option value="text.." onChange = { (e) => onInputChange(e)}>2</option>
                    <option value="eg.." onChange = { (e) => onInputChange(e)}>3</option>
                </select>
                </div>
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="First Name *"
                    name="firstname"
                    value={firstname}
                    onChange = { (e) => onInputChange(e)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Last Name *"
                    name="lastname"
                    value={lastname}
                    onChange = { (e) => onInputChange(e)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    value={email}
                    onChange = { (e) => onInputChange(e)}
                    required
                />
            </Form.Group>

            <Form.Group>
                <Form.Control
                    type="date"
                    placeholder="Date of Birth"
                    name="date"
                    value={date}
                    onChange = { (e) => onInputChange(e)}
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    as="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    value={address}
                    onChange = { (e) => onInputChange(e)}
                />
            </Form.Group>
          
            <Button variant="success" type="submit" block>
                Add 
            </Button>
        </Form>

     )
}

export default AddForm;